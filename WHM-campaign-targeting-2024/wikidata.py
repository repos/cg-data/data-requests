from typing import TypedDict

import pyspark.sql.functions as F
#import reverse_geocoder as rg
from pyspark.sql import DataFrame, SparkSession


def wikidata_relevant_qitems_df(
    spark: SparkSession,
    wikidata_snapshot: str,
    mediawiki_snapshot: str,
    projects: list[str] | set[str] | None = None,
    mw_project_namespace_map_table: str = "wmf_raw.mediawiki_project_namespace_map",
    wd_item_page_link_table: str = "wmf.wikidata_item_page",
) -> DataFrame:
    """Create a DataFrame with single column named `qitem_id`,
    containing Wikidata item IDs that have at least one Wikipedia
    sitelink from `projects`.
    """
    query = f"""
        SELECT DISTINCT(dbname) AS wiki_db
        FROM {mw_project_namespace_map_table}
        WHERE
          snapshot = '{mediawiki_snapshot}'
          AND hostname LIKE '%.wikipedia.org'"""
    relevant_wikis = spark.sql(query)
    if projects:
        relevant_wikis = relevant_wikis.where(F.col("wiki_db").isin(projects))
    relevant_wikis.createOrReplaceTempView("relevant_wikis")
    query = f"""
        SELECT DISTINCT(item_id) AS qitem_id
        FROM {wd_item_page_link_table} wd
        INNER JOIN relevant_wikis db
          ON (wd.wiki_db = db.wiki_db)
        WHERE
          snapshot = '{wikidata_snapshot}'
          AND page_namespace = 0"""
    qitems_df = spark.sql(query)
    return qitems_df


def wikidata_relevant_properties(
    spark: SparkSession,
    wikidata_snapshot: str,
    relevant_qids: DataFrame,
    wd_entity_table: str = "wmf.wikidata_entity",
) -> DataFrame:
    """Given Wikidata item IDs `relevant_qids`, explode their Wikidata
    entity data to one property-value per row.

    Returns:
        root
        |-- qitem_id: string
        |-- property: string
        |-- value: string
    """
    relevant_qids.createOrReplaceTempView("relevant_qids")
    query = f"""
    SELECT id AS qitem_id, claim.mainSnak.property,
        claim.mainsnak.datavalue.value
    FROM {wd_entity_table} w
    INNER JOIN relevant_qids q
      ON (w.id = q.qitem_id)
    LATERAL VIEW explode(claims) AS claim
    WHERE w.typ = 'item'
        AND w.snapshot = '{wikidata_snapshot}'"""
    return spark.sql(query)


def append_is_human(qids: DataFrame, wikidata_properties: DataFrame) -> DataFrame:
    """Add a boolean column `is_human`
    to DataFrame `qitems`. True if the
    item has property `instance of` (P31)
    and contains value `human` (Q5).
    """
    prop = (
        wikidata_properties.filter(F.col("property") == "P31")
        .withColumn("value", F.json_tuple("value", "id"))
        .groupby("qitem_id")
        .agg(F.collect_list("value").alias("instance_of"))
        # Q5: human
        .withColumn("is_human", F.array_contains(F.col("instance_of"), "Q5"))
        .select("qitem_id", "is_human")
    )
    df = (
        qids.alias("q")
        .join(prop.alias("p"), F.col("q.qitem_id") == F.col("p.qitem_id"), "left")
        .select("q.*", "p.is_human")
    )
    df = df.fillna(False, ["is_human"])
    return df


def append_gender_feature(
    qids: DataFrame,
    wikidata_properties: DataFrame,
    qid_to_gender: DataFrame,
) -> DataFrame:
    """Add a column `gender` to DataFrame
    `qitems`. Null if the Wikidata item doesn't
    have property `P31 (sex or gender)`.

    Returns:
        root
        |-- gender: array
        |    |-- element: struct
        |    |    |-- gender_qid: string
        |    |    |-- gender: string
    """
    prop = (
        wikidata_properties.filter(F.col("property") == "P21")
        .withColumn("gender_qid", F.json_tuple("value", "id"))
        # TODO Aiko, should this be an inner join?
        .join(qid_to_gender.alias("q"), F.col("q.qid") == F.col("gender_qid"), "left")
        .withColumn("gender", F.struct(F.col("gender_qid"), F.col("q.gender")))
        .select("qitem_id", "gender")
        .groupby("qitem_id")
        .agg(F.collect_set("gender").alias("gender"))
    )
    return (
        qids.alias("q")
        .join(prop.alias("g"), F.col("q.qitem_id") == F.col("g.qitem_id"), "left")
        .select("q.*", "g.gender")
    )


def append_sexual_orientation_feature(
    qids: DataFrame,
    wikidata_properties: DataFrame,
    qid_to_label: DataFrame,
) -> DataFrame:
    """Add a column `sexual_orientation_gap` to
    DataFrame `qitems`. Null if the Wikidata item
    does not have property `P91 (sexual orientation)`.

    Returns:
        root
        |-- sexual_orientation: array
        |    |-- element: struct
        |    |    |-- sexual_orientation_qid: string
        |    |    |-- sexual_orientation: string
    """
    prop = (
        wikidata_properties.filter(F.col("property") == "P91")
        .withColumn("sexual_orientation_qid", F.json_tuple("value", "id"))
        .join(
            qid_to_label.alias("q"),
            F.col("q.qid") == F.col("sexual_orientation_qid"),
            "left",
        )
        .withColumn(
            "sexual_orientation",
            F.struct(F.col("sexual_orientation_qid"), F.col("q.sexual_orientation")),
        )
        .select("qitem_id", "sexual_orientation")
        .groupby("qitem_id")
        .agg(F.collect_set("sexual_orientation").alias("sexual_orientation"))
    )
    return (
        qids.alias("q")
        .join(prop.alias("s"), F.col("q.qitem_id") == F.col("s.qitem_id"), "left")
        .select("q.*", "s.sexual_orientation")
    )


class Address(TypedDict):
    country_code: str
    admin1: str
    admin2: str


def reverse_geocode(lon: float, lat: float) -> Address | None:
    """Reverse Geocoder takes a latitude and longitude
    coordinate and returns the nearest town/city,
    country code, and the administrative 1 & 2 regions.
    """
    try:
        res = rg.search((lat, lon))
        return {
            "country_code": res[0]["cc"],
            "admin1": res[0]["admin1"],
            "admin2": res[0]["admin2"],
        }
    except Exception:
        return None


def geospatial_model(
    wikidata_properties: DataFrame, geography_mappings: DataFrame
) -> DataFrame:
    """
    Returns:
        root
        |-- geospatial_model: array
        |    |-- element: struct
        |    |    |-- geocoordinates: struct
        |    |    |    |-- lat: float
        |    |    |    |-- lon: float
        |    |    |-- reverse_geocode: struct
        |    |    |    |-- country_code: string
        |    |    |    |-- admin1: string
        |    |    |    |-- admin2: string
        |    |    |-- un_continent: string
        |    |    |-- un_subcontinent: string
        |    |    |-- wikimedia_region: string
    """
    reverse_geocode_udf = F.udf(
        lambda lon, lat: reverse_geocode(lon, lat),
        "struct<country_code:string,admin1:string,admin2:string>",
    )
    prop = (
        wikidata_properties.filter(F.col("property") == "P625")
        .select(
            F.col("qitem_id"), F.json_tuple(F.col("value"), "latitude", "longitude")
        )
        .withColumn(
            "geocoordinates",
            F.struct(
                F.col("c0").cast("float").alias("lat"),
                F.col("c1").cast("float").alias("lon"),
            ),
        )
        .withColumn(
            "geocode",
            reverse_geocode_udf(
                F.col("geocoordinates.lon"), F.col("geocoordinates.lat")
            ),
        )
        .drop("c0", "c1")
        .join(
            geography_mappings.alias("m"),
            F.col("m.iso_code") == F.col("geocode.country_code"),
            "left",
        )
        .withColumn(
            "geospatial_model",
            F.struct(
                F.col("geocoordinates"),
                F.col("geocode"),
                F.col("m.un_continent"),
                F.col("m.un_subcontinent"),
                F.col("m.wikimedia_region"),
            ),
        )
        .select("qitem_id", "geospatial_model")
        .groupby("qitem_id")
        .agg(F.collect_set("geospatial_model").alias("geospatial_model"))
    )
    return prop


def cultural_model(
    wikidata_properties: DataFrame,
    geography_mappings: DataFrame,
    territories_aggregation: DataFrame,
    region_properties: list[str] | set[str],
) -> DataFrame:
    """
    Returns:
        root
        |-- qitem_id: string (nullable = true)
        |-- cultural_model: array (nullable = false)
        |    |-- element: struct (containsNull = false)
        |    |    |-- property: string (nullable = true)
        |    |    |-- value: string (nullable = true)
        |    |    |-- iso_code: string (nullable = true)
        |    |    |-- un_continent: string (nullable = true)
        |    |    |-- un_subcontinent: string (nullable = true)
        |    |    |-- wikimedia_region: string (nullable = true)
    """

    # the coverage is expanded with smaller territories by associating
    # them with the relevant canonical wmf geo entity
    expanded = territories_aggregation.join(
        geography_mappings, on=F.col("to_qid") == F.col("wikidata_id")
    ).select(
        F.col("from_qid").alias("wikidata_id"),
        F.col("iso_code"),
        F.col("un_continent"),
        F.col("un_subcontinent"),
        F.col("wikimedia_region"),
    )
    cols = expanded.columns
    expanded_geography_mappings = geography_mappings.select(cols).union(expanded)

    prop = (
        wikidata_properties.filter(F.col("property").isin(region_properties))
        .withColumn("qid", F.json_tuple("value", "id"))
        .join(
            expanded_geography_mappings,
            F.col("wikidata_id") == F.col("qid"),
        )
        .withColumn(
            "cultural_model",
            F.struct(F.col("property"), *cols),
        )
        .select("qitem_id", "cultural_model")
        .groupby("qitem_id")
        .agg(F.collect_set("cultural_model").alias("cultural_model"))
    )
    return prop


def append_geographic_feature(
    qids: DataFrame,
    wikidata_properties: DataFrame,
    geography_mappings: DataFrame,
    territories_aggregation: DataFrame,
    region_properties: list[str] | set[str],
) -> DataFrame:
    geospatial_item = geospatial_model(wikidata_properties, geography_mappings)

    cultural_item = cultural_model(
        wikidata_properties,
        geography_mappings,
        territories_aggregation,
        region_properties,
    )
    items = (
        qids.alias("q")
        .join(
            geospatial_item.alias("g"),
            F.col("q.qitem_id") == F.col("g.qitem_id"),
            "left",
        )
        .join(
            cultural_item.alias("r"), F.col("q.qitem_id") == F.col("r.qitem_id"), "left"
        )
        .select("q.*", "g.geospatial_model", "r.cultural_model")
        .withColumn(
            "geographic", F.struct(F.col("geospatial_model"), F.col("cultural_model"))
        )
        .withColumn(
            "geographic",
            F.when(
                F.col("geographic.geospatial_model").isNull()
                & F.col("geographic.cultural_model").isNull(),
                None,
            ).otherwise(F.col("geographic")),
        )
        .drop("geospatial_model", "cultural_model")
    )
    return items


def get_year(time: str) -> int | None:
    """Get year from a timestamp. The year is always
    signed and padded to have between 4 and 16 digits
    """
    year = None
    if time:
        year = int(time[0] + time[1:].split("-")[0])
    return year


def append_time_feature(
    qids: DataFrame,
    wikidata_properties: DataFrame,
    time_properties: list[str] | set[str],
) -> DataFrame:
    """
    Returns:
        root
        |-- time: array
        |    |-- element: struct
        |    |    |-- property: string
        |    |    |-- year: integer
    """
    get_year_udf = F.udf(lambda time: get_year(time), "int")
    prop = (
        wikidata_properties.filter(F.col("property").isin(time_properties))
        .withColumn("time", F.json_tuple("value", "time"))
        .withColumn("year", get_year_udf(F.col("time")))
        .withColumn("time", F.struct(F.col("property"), F.col("year")))
        .select("qitem_id", "time")
        .groupby("qitem_id")
        .agg(F.collect_set("time").alias("time"))
    )
    return (
        qids.alias("q")
        .join(prop.alias("t"), F.col("q.qitem_id") == F.col("t.qitem_id"), "left")
        .select("q.*", "t.time")
    )
